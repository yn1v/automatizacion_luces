#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Librerias
from gpiozero import Button
from gpiozero import LED
from signal import pause
from time import sleep, strftime, localtime
import schedule
import logging
import telegram
from telegram.error import NetworkError, Unauthorized

# Conecciones al GPIO
relay1 = LED(10)
relay2 = LED(9)
relay3 = LED(11)
relay4 = LED(5)
relay5 = LED(6)
relay6 = LED(13)
relay7 = LED(19)
relay8 = LED(26)

boton1 = Button(24, bounce_time=0.1)
boton2 = Button(25, bounce_time=0.1)
boton3 = Button(8, bounce_time=0.1)
boton4 = Button(7, bounce_time=0.1)
boton5 = Button(12, bounce_time=0.1)
boton6 = Button(16, bounce_time=0.1)
boton7 = Button(20, bounce_time=0.1)
boton8 = Button(21, bounce_time=0.1)

startup = 0

def log_entry(msg):
    time_stamp = strftime("%Y %m %d %H:%M:%S", localtime())
    log = open("log_relay.txt","a")
    log.write(time_stamp + " " + msg + "\n")
    log.close()

def cambiar1():
    relay1.toggle()
    log_entry("Cambiar 1")

def cambiar2():
    relay2.toggle()
    log_entry("Cambiar 2")

def cambiar3():
    relay3.toggle()
    log_entry("Cambiar 3")

def cambiar4():
    relay4.toggle()
    log_entry("Cambiar 4")

def cambiar5():
    relay5.toggle()
    log_entry("Cambiar 5")

def cambiar6():
    relay6.toggle()
    log_entry("Cambiar 6")

def cambiar7():
    relay7.toggle()
    log_entry("Cambiar 7")

def cambiar8():
    relay8.toggle()
    log_entry("Cambiar 8")


def encender1():
    relay1.on()
    log_entry("Encender 1")

def apagar1():
    relay1.off()
    log_entry("Apagar 1")

def encender2():
    relay2.on()
    log_entry("Encender 2")


def apagar2():
    relay2.off()
    log_entry("Apagar 2")

def encender3():
    relay3.on()
    log_entry("Encender 3")

def apagar3():
    relay3.off()
    log_entry("Apagar 3")

def encender4():
    relay4.on()
    log_entry("Encender 4")

def apagar4():
    relay4.off()
    log_entry("Apagar 4")

def encender5():
    relay5.on()
    log_entry("Encender 5")

def apagar5():
    relay5.off()
    log_entry("Apagar 5")

def encender6():
    relay6.on()
    log_entry("Encender 6")

def apagar6():
    relay6.off()
    log_entry("Apagar 6")

def encender7():
    relay7.on()
    log_entry("Encender 7")

def apagar7():
    global status7
    relay7.off()
    status7=0
    log_entry("Apagar 7")

def encender8():
    relay8.on()
    log_entry("Encender 8")

def apagar8():
    relay8.off()
    log_entry("Apagar 8")

def debug():
    time_stamp = strftime("%Y %m %d %H:%M:%S", localtime())
    log_bug = open("log_debug_relay.txt","a")
    log_bug.write(time_stamp + " script activo\n")
    log_bug.close()

# solo se ejecuta una vez al inicio
# print("starting up")
if startup == 0:
    relay1.off()
    sleep(1)
    relay2.off()
    sleep(1)
    relay3.off()
    sleep(1)
    relay4.off()
    sleep(1)
    relay5.off()
    sleep(1)
    relay6.off()
    sleep(1)
    relay7.off()
    sleep(1)
    relay8.off()
    log_entry("Ready")
    # cambiar a estado a ya ejecutado
    startup = 1
    # Encender luces si es de noche
    now_str = strftime("%H:%M", localtime())
    if now_str > "17:00" or now_str < "06:00":
        encender1()
#       encender5()

        

# Agenda de operaciones segun horario
# print("starting schedule")
schedule.every().day.at("6:30").do(apagar1)
schedule.every().day.at("16:45").do(encender1)
# schedule.every().day.at("8:00").do(encender6)
# schedule.every().day.at("22:00").do(apagar6)
# schedule.every().day.at("16:46").do(encender5)
# schedule.every().day.at("8:00").do(apagar5)
# schedule.every().day.at("16:30").do(apagar7)
# schedule.every(23).minutes.do(cambiar4)
# schedule.every(10).minutes.do(debug) # Programa activo


# Bucle infinito
# print("bucle while")
def relay_control():
    schedule.run_pending()
    boton1.when_pressed=cambiar1
    boton2.when_pressed=cambiar2
    boton3.when_pressed=cambiar3
    boton4.when_pressed=cambiar4
    boton5.when_pressed=cambiar5
    boton6.when_pressed=cambiar6
    boton7.when_pressed=cambiar7
    boton8.when_pressed=cambiar8


update_id = None

autorized_users = ["Neville_yn1v"]

def main():
    """Run the bot."""
    global update_id
    # Telegram Bot Authorization Token
    bot = telegram.Bot('844753256:AAFEqEKBLoASCIJf6Qi_N-yG29kqLYHv-c8')

    # get the first pending update_id, this is so we can skip over it in case
    # we get an "Unauthorized" exception.
    try:
        update_id = bot.get_updates()[0].update_id
    except IndexError:
        update_id = None

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    while True:
        try:
            echo(bot)
        except NetworkError:
            sleep(1)
        except Unauthorized:
            # The user has removed or blocked the bot.
            update_id += 1


def echo(bot):
    relay_control()
    """Echo the message the user sent."""
    global update_id
    # Request updates after the last update_id
    for update in bot.get_updates(offset=update_id, timeout=10):
        update_id = update.update_id + 1
        
        if update.message.chat.username in autorized_users:
            if update.message.text.lower() == "hola":
                update.message.reply_text("Como estas?")
            elif update.message.text == "1":
                cambiar1()
                if relay1.is_lit:
                    update.message.reply_text("Circuito 1 encendido")
                else:
                    update.message.reply_text("Circuito 1 apagado")
            elif update.message.text == "2":
                cambiar2()
                if relay2.is_lit:
                    update.message.reply_text("Circuito 2 encendido")
                else:
                    update.message.reply_text("Circuito 2 apagado")
            elif update.message.text == "3":
                cambiar3()
                if relay3.is_lit:
                    update.message.reply_text("Circuito 3 encendido")
                else:
                    update.message.reply_text("Circuito 3 apagado")
            elif update.message.text == "4":
                cambiar4()
                if relay4.is_lit:
                    update.message.reply_text("Circuito 4 encendido")
                else:
                    update.message.reply_text("Circuito 4 apagado")
            elif update.message.text == "5":
                cambiar5()
                if relay5.is_lit:
                    update.message.reply_text("Circuito 5 encendido")
                else:
                    update.message.reply_text("Circuito 5 apagado")
            elif update.message.text == "6":
                cambiar6()
                if relay6.is_lit:
                    update.message.reply_text("Circuito 6 encendido")
                else:
                    update.message.reply_text("Circuito 6 apagado")
            elif update.message.text == "7":
                cambiar7()
                if relay7.is_lit:
                    update.message.reply_text("Circuito 7 encendido")
                else:
                    update.message.reply_text("Circuito 7 apagado")
            elif update.message.text == "8":
                cambiar8()
                if relay8.is_lit:
                    update.message.reply_text("Circuito 8 encendido")
                else:
                    update.message.reply_text("Circuito 8 apagado")
            else:
                update.message.reply_text("No se que hacer con esa información")
        else:
            update.message.reply_text("Lo siento, "+ update.message.first_name + "pero yo no te conozco")


if __name__ == '__main__':
    main()
