from gpiozero import Button
from gpiozero import LED
from signal import pause
from time import sleep
import schedule

azul = LED(17)
boton = Button(18, bounce_time=0.1)
azulStatus = 0

print("Ready")

def cambiar():
    global azulStatus
    if azulStatus == 0:
        azul.on()
        azulStatus=1
        print("Encendido")
    else:
        azul.off()
        azulStatus=0
        print("apagado")

def encender():
    global azulStatus
    azul.on()
    azulStatus=1
    print("def encender")

def apagar():
    global azulStatus
    azul.off()
    azulStatus=0
    print("def apagar")


#schedule.every(3).minutes.do(cambiar)
schedule.every().sunday.at("13:37").do(encender)
schedule.every().sunday.at("13:38").do(apagar)
while True:
    schedule.run_pending()
    boton.when_pressed=cambiar
    sleep(1)
