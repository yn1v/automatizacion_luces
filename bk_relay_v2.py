from gpiozero import Button
from gpiozero import LED
from signal import pause
from time import sleep
import schedule

relay1 = LED(2)
relay2 = LED(3)
relay3 = LED(4)
relay4 = LED(17)
relay5 = LED(27)
relay6 = LED(22)
relay7 = LED(10)
relay8 = LED(9)

boton1 = Button(14, bounce_time=0.1)
boton2 = Button(15, bounce_time=0.1)
boton3 = Button(18, bounce_time=0.1)
boton4 = Button(23, bounce_time=0.1)
boton5 = Button(24, bounce_time=0.1)
boton6 = Button(25, bounce_time=0.1)
boton7 = Button(8, bounce_time=0.1)
boton8 = Button(7, bounce_time=0.1)

status1 = 1
status2 = 1
status3 = 1
status4 = 1
status5 = 1
status6 = 1
status7 = 1
status8 = 1

relay1.on()
sleep(1)
relay2.on()
sleep(1)
relay3.on()
sleep(1)
relay4.on()
sleep(1)
relay5.on()
sleep(1)
relay6.on()
sleep(1)
relay7.on()
sleep(1)
relay8.on()

print("Ready")

def cambiar1():
    global status1
    if status1 == 0:
        relay1.on()
        status1=1
        print("Apagado 1")
    else:
        relay1.off()
        status1=0
        print("Encendido 1")

def cambiar2():
    global status2
    if status2 == 0:
        relay2.on()
        status2=1
        print("Apagado 2")
    else:
        relay2.off()
        status2=0
        print("Encendido 2")

def cambiar3():
    global status3
    if status3 == 0:
        relay3.on()
        status3=1
        print("Apagado 3")
    else:
        relay3.off()
        status3=0
        print("Encendido 3")

def cambiar4():
    global status4
    if status4 == 0:
        relay4.on()
        status4=1
        print("Apagado 4")
    else:
        relay4.off()
        status4=0
        print("Encendido 4")

def cambiar5():
    global status5
    if status5 == 0:
        relay5.on()
        status5=1
        print("Apagado 5")
    else:
        relay5.off()
        status5=0
        print("Encendido 5")

def cambiar6():
    global status6
    if status6 == 0:
        relay6.on()
        status6=1
        print("Apagado 6")
    else:
        relay6.off()
        status6=0
        print("Encendido 6")

def cambiar7():
    global status7
    if status7 == 0:
        relay7.on()
        status7=1
        print("Apagado 7")
    else:
        relay7.off()
        status7=0
        print("Encendido 7")

def cambiar8():
    global status8
    if status8 == 0:
        relay8.on()
        status8=1
        print("Apagado 8")
    else:
        relay8.off()
        status8=0
        print("Encendido 8")



def encender1():
    global status1
    relay1.off()
    status1=1
    print("Encender 1")

def apagar1():
    global status1
    relay1.on()
    status1=0
    print("Apagar 1")

def encender2():
    global status2
    relay2.off()
    status2=1
    print("Encender 2")

def apagar2():
    global status2
    relay2.on()
    status2=0
    print("Apagar 2")

def encender3():
    global status3
    relay3.off()
    status3=1
    print("Encender 3")

def apagar3():
    global status3
    relay3.on()
    status3=0
    print("Apagar 3")

def encender4():
    global status4
    relay4.off()
    status4=1
    print("Encender 4")

def apagar4():
    global status4
    relay4.on()
    status4=0
    print("Apagar 4")

def encender5():
    global status5
    relay5.off()
    status5=1
    print("Encender 5")

def apagar5():
    global status5
    relay5.on()
    status5=0
    print("Apagar 5")

def encender6():
    global status6
    relay6.off()
    status6=1
    print("Encender 6")

def apagar6():
    global status6
    relay6.on()
    status6=0
    print("Apagar 6")

def encender7():
    global status7
    relay7.off()
    status7=1
    print("Encender 7")

def apagar7():
    global status7
    relay7.on()
    status7=0
    print("Apagar 7")

def encender8():
    global status8
    relay8.off()
    status8=1
    print("Encender 8")

def apagar8():
    global status8
    relay8.on()
    status8=0
    print("Apagar 8")


schedule.every().day.at("6:30").do(apagar1)
schedule.every().day.at("17:00").do(encender1)
schedule.every().day.at("8:00").do(encender6)
schedule.every().day.at("22:00").do(apagar6)
schedule.every().day.at("17:00").do(encender5)
schedule.every().day.at("8:00").do(apagar5)
schedule.every().day.at("18:00").do(apagar7)

while True:
    schedule.run_pending()
    boton1.when_pressed=cambiar1
    boton2.when_pressed=cambiar2
    boton3.when_pressed=cambiar3
    boton4.when_pressed=cambiar4
    boton5.when_pressed=cambiar5
    boton6.when_pressed=cambiar6
    boton7.when_pressed=cambiar7
    boton8.when_pressed=cambiar8
    sleep(1)
