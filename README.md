# automatizacion_luces

Esta es una prueba de automatización de luces.

requiere instalar las librerias de python gpiozero y schedule

sudo apt-get install python3-gpiozero python3-schedule

el archivo relaybox.service de copiarse en la ruta

/etc/systemd/system/

IMPORTANTE: el script se encuentra en el home del usuario pi, si usa otra ruta debe corregirla en el archivo de servicio

para asegurarse que el script va a correr al bootear el sistema operativo puede usar:

sudo systemctl enable relaybox.service

para iniciar el script puede usar

sudo systemctl start relaybox.service

NOTA: Raspbian corre en UTC a menos que se le cambie la zona horaria. Schedule va a ejecutarse segun la hora del sistema operativo. Es recomendable ajustar la localización del sistema para incluir la zona horaria. O bien, introducir los valores en schedule en UTC. Tome en cuenta que un raspberry pi no tiene reloj. Si el equipo no se conecta a internet nunca se actualizará ntp. Cuando apaga el raspberry pi, guarda la ultima hora en un reloj falso y lo usa en caso de que ntp falle. Es posible que para esta aplicación sea conveniente usar un modulo de RTC (real time clock) reloj de tiempo real.


El banco de relays usa una señal LOW para activar los relay. Esto significa que al cargar el script e iniciar la conexion al GPIO todos los pines reciben la señal apagado LOW, que los relay interpretan como encendido. Si bien es factible invertir la programacion para usar on (HIGH) como apagado. El estado inicial del sistema, es mejor que no pase energía. Para ello se ha includo un conjunto de transistores npn.

https://www.raspberrypi.org/forums/viewtopic.php?f=44&t=36225

Software Troubleshooting.
1) verifique que el servicio esta activo.
sudo systemctl status relaybox.service

Si no esta activo:
a) hizo que el servicio cargara al iniciar el sistema?
sudo systemctl enable relaybox.service

b) el servicio falla al cargar.
Verifique la ruta del script en el archivo de servicio

MQTT
Los modulos DS3231 tienen un sensor de temperatura que se puede aprovechar para obtener datos del ambiente. Tambien se puede usar un sensor DS18B20. Para esto se han incluido dos ejemplos que leen la temperatura y luego la envian via MQTT. Para esto es necesario instalar la libreria python3-paho-mqtt
