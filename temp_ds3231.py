# Temperatura RTC DS3231
# librerias
import time
import paho.mqtt.client as mqtt

device_file = "/sys/bus/i2c/devices/1-0068/hwmon/hwmon2/temp1_input"

#obtener datos del RTC
def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

#obtener la temperatura
def read_temp():
    lines = read_temp_raw()
    value = str(lines[0])
    string_value = value.strip()
    temp_c = float(string_value)/1000.0
    return temp_c

broker_address="zancudo.sytes.net"
client = mqtt.Client("front-desk")
ruta = "teodolinda/recepcion/temp"

while True:
    datos = str(read_temp())
    client.connect(broker_address)
    client.publish(ruta, datos)
    print(datos, broker_address, ruta)
    client.disconnect()
    time.sleep(60)

