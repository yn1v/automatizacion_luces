# automatizacion_luces

Esta es una prueba de automatización de luces.

2018/08/29
Primer cambio. Se ingreso una codicional para chequear si estaba corriendo por primera vez el programa y encender las luces dentro de un rango de hora. Esto soluciona el problema que si el equipo se reinicia en la noche, las luces permanecen apagadas.

Segundo cambio. Se creo una funsion de escritura al log, lo que significa reducir 4 lineas por accion, son casi 100 lineas de código menos!!

----------------------

2018/08/23
Puse un conjunto de transistores npn para evitar la progrmación invertida. El relay recibe una señal baja para activarse entonces off = activo. Ahora on = activo. Esto para evitar que los relay queden activos si el sistema pierde el control.

----------------------

2018/08/16
Separe el archivo de log en dos. Uno para verificar que esta activo (debug) y otro para registrar los eventos. Cambie el nombre para que los logs inicien con la palabra log.

----------------------
2018/08/15
El uso de /etc/rc.local hacia que el programa no funcionara adecuadamente. 
La sección incluida para verifación que estaba ejecutandose cada 10 minutos a veces no llegaba a ejecutarse la primera vez. 
Se crea el archivo relaybox.service a incluirse en /etc/systemd/system/ de forma que systemd tome control del script.
Gracias a Adolfo Fitoria por la sugerencia.

----------------------
Esta corriendo en un raspberry pi v2.0 B. Es un modelo viejo con 512Mb de Memoria. Tiene raspbian actualizado.

El script de python corre al inicializar el raspberry pi usando /etc/rc.local

v0 = Prueba de control de gpio y schedule
v1 = Prueba de pines y (8 de entrada y 8 de salida)
v2 = Creando todas las opciones de apagar y encender
actual = Pasado a git. Se incluye log de acciones
