from gpiozero import Button
from gpiozero import LED
from signal import pause
from time import sleep
import schedule

led1 = LED(2)
led2 = LED(3)
led3 = LED(4)
led4 = LED(17)
led5 = LED(27)
led6 = LED(22)
led7 = LED(10)
led8 = LED(9)

boton1 = Button(14, bounce_time=0.1)
boton2 = Button(15, bounce_time=0.1)
boton3 = Button(18, bounce_time=0.1)
boton4 = Button(23, bounce_time=0.1)
boton5 = Button(24, bounce_time=0.1)
boton6 = Button(25, bounce_time=0.1)
boton7 = Button(8, bounce_time=0.1)
boton8 = Button(7, bounce_time=0.1)

status1 = 0
status2 = 0
status3 = 0
status4 = 0
status5 = 0
status6 = 0
status7 = 0
status8 = 0

print("Ready")

def cambiar1():
    global status1
    if status1 == 0:
        led1.on()
        status1=1
        print("Encendido 1")
    else:
        led1.off()
        status1=0
        print("Apagado 1")

def cambiar2():
    global status2
    if status2 == 0:
        led2.on()
        status2=1
        print("Encendido 2")
    else:
        led2.off()
        status2=0
        print("Apagado 2")

def cambiar3():
    global status3
    if status3 == 0:
        led3.on()
        status3=1
        print("Encendido 3")
    else:
        led3.off()
        status3=0
        print("Apagado 3")

def cambiar4():
    global status4
    if status4 == 0:
        led4.on()
        status4=1
        print("Encendido 4")
    else:
        led4.off()
        status4=0
        print("Apagado 4")

def cambiar5():
    global status5
    if status5 == 0:
        led5.on()
        status5=1
        print("Encendido 5")
    else:
        led5.off()
        status5=0
        print("Apagado 5")

def cambiar6():
    global status6
    if status6 == 0:
        led6.on()
        status6=1
        print("Encendido 6")
    else:
        led6.off()
        status6=0
        print("Apagado 6")

def cambiar7():
    global status7
    if status7 == 0:
        led7.on()
        status7=1
        print("Encendido 7")
    else:
        led7.off()
        status7=0
        print("Apagado 7")

def cambiar8():
    global status8
    if status8 == 0:
        led8.on()
        status8=1
        print("Encendido 8")
    else:
        led8.off()
        status8=0
        print("Apagado 8")



def encender():
    global azulStatus
    azul.on()
    azulStatus=1
    print("def encender")

def apagar():
    global azulStatus
    azul.off()
    azulStatus=0
    print("def apagar")


#schedule.every(3).minutes.do(cambiar)
schedule.every().sunday.at("13:37").do(encender)
schedule.every().sunday.at("13:38").do(apagar)
while True:
    schedule.run_pending()
    boton1.when_pressed=cambiar1
    boton2.when_pressed=cambiar2
    boton3.when_pressed=cambiar3
    boton4.when_pressed=cambiar4
    boton5.when_pressed=cambiar5
    boton6.when_pressed=cambiar6
    boton7.when_pressed=cambiar7
    boton8.when_pressed=cambiar8
    sleep(1)
