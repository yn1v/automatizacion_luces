# Librerias
from gpiozero import Button
from gpiozero import LED
from signal import pause
from time import sleep, strftime, localtime
import schedule

# Variables
startup = 0

# Conecciones al GPIO
relay1 = LED(26)
relay2 = LED(19) 
relay3 = LED(22)
relay4 = LED(27)
relay5 = LED(13)
relay6 = LED(6)
relay7 = LED(5)
relay8 = LED(17)

boton1 = Button(18, bounce_time=0.1)
boton2 = Button(23, bounce_time=0.1)
boton3 = Button(24, bounce_time=0.1)
boton4 = Button(25, bounce_time=0.1)
boton5 = Button(12, bounce_time=0.1)
boton6 = Button(16, bounce_time=0.1)
boton7 = Button(20, bounce_time=0.1)
boton8 = Button(21, bounce_time=0.1)


def log_entry(msg):
    time_stamp = strftime("%Y %m %d %H:%M:%S", localtime())
    log = open("log_relay.txt","a")
    log.write(time_stamp + " " + msg + "\n")
    log.close()

def cambiar1():
    print("uno")
    relay1.toggle()
    log_entry("Cambiar 1")

def cambiar2():
    print("dos")
    relay2.toggle()
    log_entry("Cambiar 2")

def cambiar3():
    print("tres")
    relay3.toggle()
    log_entry("Cambiar 3")

def cambiar4():
    print("cuatro")
    relay4.toggle()
    log_entry("Cambiar 4")

def cambiar5():
    print("cinco")
    relay5.toggle()
    log_entry("Cambiar 5")

def cambiar6():
    print("seis")
    relay6.toggle()
    log_entry("Cambiar 6")

def cambiar7():
    print("siete")
    relay7.toggle()
    log_entry("Cambiar 7")

def cambiar8():
    print("ocho")
    relay8.toggle()
    log_entry("Cambiar 8")

def encender1():
    relay1.on()
    log_entry("Encender 1")

def apagar1():
    relay1.off()
    log_entry("Apagar 1")

def encender2():
    relay2.on()
    log_entry("Encender 2")

def apagar2():
    relay2.off()
    log_entry("Apagar 2")

def encender3():
    relay3.on()
    log_entry("Encender 3")

def apagar3():
    relay3.off()
    log_entry("Apagar 3")

def encender4():
    relay4.on()
    log_entry("Encender 4")

def apagar4():
    relay4.off()
    log_entry("Apagar 4")

def encender5():
    relay5.on()
    log_entry("Encender 5")

def apagar5():
    relay5.off()
    log_entry("Apagar 5")

def encender6():
    relay6.on()
    log_entry("Encender 6")

def apagar6():
    relay6.off()
    log_entry("Apagar 6")

def encender7():
    relay7.on()
    log_entry("Encender 7")

def apagar7():
    relay7.off()
    log_entry("Apagar 7")

def encender8():
    relay8.on()
    log_entry("Encender 8")

def apagar8():
    relay8.off()
    log_entry("Apagar 8")

def debug():
    time_stamp = strftime("%Y %m %d %H:%M:%S", localtime())
    log_bug = open("log_debug_relay.txt","a")
    log_bug.write(time_stamp + " script activo\n")
    log_bug.close()

# solo se ejecuta una vez al inicio
# print("starting up")
if startup == 0:
#    encender1()
    relay1.off()
    sleep(1)
    relay2.off()
    sleep(1)
    relay3.off()
    sleep(1)
    relay4.off()
    sleep(1)
    relay5.off()
    sleep(1)
    relay6.off()
    sleep(1)
    relay7.off()
    sleep(1)
    relay8.off()
    log_entry("Ready")
    # cambiar a estado a ya ejecutado
    startup = 1
    # Encender luces si es de noche
    now_str = strftime("%H:%M", localtime())
    if now_str > "17:45" or now_str < "05:45":
        encender1()
#        encender2()
#        encender3()
#       encender5()

        

# Agenda de operaciones segun horario
# print("starting schedule")
schedule.every().day.at("5:45").do(apagar1)
schedule.every().day.at("17:45").do(encender1)
#schedule.every().day.at("6:30").do(apagar2)
#schedule.every().day.at("16:45").do(encender2)
#schedule.every().day.at("6:30").do(apagar3)
#schedule.every().day.at("16:45").do(encender3)
# schedule.every(23).minutes.do(cambiar4)
#schedule.every(10).minutes.do(debug) # Programa activo


# Bucle infinito
# print("bucle while")
while True:
    schedule.run_pending()
    boton1.when_pressed=cambiar1
    boton2.when_pressed=cambiar2
    boton3.when_pressed=cambiar3
    boton4.when_pressed=cambiar4
    boton5.when_pressed=cambiar5
    boton6.when_pressed=cambiar6
    boton7.when_pressed=cambiar7
    boton8.when_pressed=cambiar8
    sleep(0.1)
